from django.shortcuts import render, redirect
from django.views.generic import *
from .forms import *
from .models import *
from django.urls import reverse_lazy, reverse
from django.http import JsonResponse
from datetime import datetime, timedelta
from django.contrib.auth import authenticate, login, logout
from .serializers import *
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView


class ClientMixin(object):
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allcategorys'] = ProductCategory.objects.filter(root=None)
        if 'cart_id' in self.request.session:
            cart_obj = Cart.objects.get(id=self.request.session['cart_id'])
            if self.request.user.is_authenticated and self.request.user.groups.filter(name="customer").exists():
                cart_obj.customer = Customer.objects.get(
                    user=self.request.user)
                cart_obj.save()

        return context


class ClientHomeView(ClientMixin, TemplateView):
    template_name = 'clienttemplates/clienthome.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if 'cat' in self.request.GET:
            category = ProductCategory.objects.get(
                slug=self.request.GET['cat'])
            context['thiscat'] = category
            context['allproducts'] = Product.objects.filter(category=category)
        else:
            context['thiscat'] = "All products"
            context['allproducts'] = Product.objects.all()
        return context

        


class ClientProductDetailView(ClientMixin, DetailView):
    template_name = 'clienttemplates/clientproductdetail.html'
    model = Product
    context_object_name = "productdetail"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reviewform'] = ReviewForm
        return context


class ClientReviewView(ClientMixin, CreateView):
    template_name = "clienttemplates/clientproductdetail.html"
    form_class = ReviewForm

    def form_valid(self, form):
        form.instance.product = Product.objects.get(id=self.kwargs['pro_id'])
        form.instance.customer = Customer.objects.get(user=self.request.user)
        return super().form_valid(form)

    def get_success_url(self):
        product_id = self.kwargs['pro_id']
        return reverse('ecomapp:clientproductdetail', kwargs={'pk': product_id})


class ClientReviewDeleteView(ClientMixin, DeleteView):
    template_name = "clienttemplates/clientreviewdelete.html"
    model = ProductReview

    def dispatch(self, request, *args, **kwargs):
        # allow delete to those who make review
        r_id = self.kwargs.get('pk')
        r_obj = ProductReview.objects.get(id=r_id)
        r_customer = r_obj.customer
        # Currently loggedin customer
        l_user = request.user
        l_customer = Customer.objects.get(user=l_user)
        if r_customer == l_customer:
            pass
        else:
            return render(request, 'clienttemplates/errorpage.html',
                          {'errordescription': 'Unauthorized access'})
        return super().dispatch(request, *args, **kwargs)

    def get_success_url(self):
        product_id = self.kwargs['pro_id']
        return reverse('ecomapp:clientproductdetail', kwargs={'pk': product_id})


class MyCartView(ClientMixin, TemplateView):
    template_name = "clienttemplates/mycart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        cart_id = self.request.session.get('cart_id')
        if cart_id:
            context['cart'] = Cart.objects.get(id=cart_id)
        else:
            print("you dont have cart id in the session.")
        return context


class AddToCartView(ClientMixin, TemplateView):
    template_name = "clienttemplates/addtocart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # get the cart id stored in a session or create a new cart
        # and store its id in session
        cart_id = self.request.session.get('cart_id')
        if cart_id:
            cart_obj = Cart.objects.get(id=cart_id)
        else:
            cart_obj = Cart.objects.create(total=0)
            self.request.session['cart_id'] = cart_obj.id

        context['cart'] = cart_obj
        # Get the product id and product from customer
        product_id = self.request.GET.get('p_id')
        product = Product.objects.get(id=product_id)
        context['product'] = product

        # Add that product in cartproduct
        cp_query = cart_obj.cartproduct_set.filter(product=product)
        if cp_query.exists():
            # what to do
            cp = cp_query.first()
            cp.quantity += 1
            cp.subtotal += product.sp
            cp.save()
            cart_obj.total += product.sp
            cart_obj.save()
        else:
            cartproduct = CartProduct.objects.create(
                cart=cart_obj, product=product, quantity=1,
                rate=product.sp, subtotal=product.sp)
            cart_obj.total += product.sp
            cart_obj.save()

        return context


class ManageCartView(TemplateView):
    template_name = "clienttemplates/managecart.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        action = self.request.GET['action']
        if action != "empty":
            cp_id = self.request.GET['cp_id']
            cp_obj = CartProduct.objects.get(id=cp_id)
            cart = cp_obj.cart
            if action == "inc":
                cp_obj.quantity += 1
                cp_obj.subtotal += cp_obj.rate
                cp_obj.save()
                cart.total += cp_obj.rate
                cart.save()

            elif action == "dec":
                cp_obj.quantity -= 1
                cp_obj.subtotal -= cp_obj.rate
                cp_obj.save()
                cart.total -= cp_obj.rate
                cart.save()
                if cp_obj.quantity < 1:
                    cp_obj.delete()

            elif action == "del":
                cart.total -= cp_obj.subtotal
                cart.save()
                cp_obj.delete()
            else:
                print("Invalid operation")
        else:
            cart = Cart.objects.get(id=self.request.GET['c_id'])
            cart.total = 0
            cart.save()
            cart.cartproduct_set.all().delete()
        context['message'] = "Cart updated successfully"
        return context


class OrderCreateView(ClientMixin, CreateView):
    template_name = "clienttemplates/ordercreate.html"
    form_class = OrderForm
    success_url = reverse_lazy('ecomapp:clienthome')

    def dispatch(self, request, *args, **kwargs):

        cart = Cart.objects.get(id=self.request.session['cart_id'])
        if cart.cartproduct_set.count() > 0:
            pass
        else:
            return redirect('ecomapp:clienthome')
        if request.user.is_authenticated and request.user.groups.filter(name="customer"):
            pass
        else:
            return redirect('ecomapp:login')
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # today = datetime.today()
        context['cart'] = Cart.objects.get(id=self.request.session['cart_id'])
        return context

    def form_valid(self, form):
        my_cart = Cart.objects.get(id=self.request.session['cart_id'])
        form.instance.cart = my_cart
        form.instance.subtotal = my_cart.total
        form.instance.discount = 0
        form.instance.total = my_cart.total
        form.instance.order_status = "Order Placed"
        form.instance.delivery_date = datetime.today() + timedelta(days=5)
        for cp in my_cart.cartproduct_set.all():
            p = cp.product
            p.quantity -= cp.quantity
            p.save()

        del self.request.session['cart_id']
        return super().form_valid(form)


class RegistrationView(ClientMixin, CreateView):
    template_name = "clienttemplates/registration.html"
    form_class = CustomerRegistrationForm
    success_url = reverse_lazy('ecomapp:clienthome')

    def form_valid(self, form):
        a = form.cleaned_data['username']
        b = form.cleaned_data['password']
        c = form.cleaned_data['email']
        puja_user = User.objects.create_user(a, c, b)
        form.instance.user = puja_user
        return super().form_valid(form)

  # Admin views


class AdminRequiredMixin(object):
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated and request.user.groups.filter(name="admin").exists():
            pass
        else:
            return redirect('ecomapp:login')
        return super().dispatch(request, *args, **kwargs)


class AdminHomeView(AdminRequiredMixin, TemplateView):
    template_name = "admintemplates/adminhome.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['orderlist'] = Order.objects.all().order_by('-id')
        return context


class AdminOrderDetailView(AdminRequiredMixin, DetailView):
    template_name = "admintemplates/adminorderdetail.html"
    model = Order
    context_object_name = "orderobj"


class LogoutView(View):
    def get(self, request):
        logout(request)
        return redirect('/')


class LoginView(ClientMixin, FormView):
    template_name = "admintemplates/adminlogin.html"
    form_class = LoginForm
    success_url = reverse_lazy('ecomapp:adminhome')

    def form_valid(self, form):
        uname = form.cleaned_data["username"]
        pword = form.cleaned_data["password"]

        user = authenticate(username=uname, password=pword)

        if user is not None:
            login(self.request, user)
        else:
            return render(self.request, "admintemplates/adminlogin.html", {
                "form": form,
                "error": "Invalid username or password"
            })

        return super().form_valid(form)

    def get_success_url(self):
        if self.request.user.groups.filter(name="admin").exists():
            return reverse('ecomapp:adminhome')
        elif self.request.user.groups.filter(name="customer").exists():
            return reverse('ecomapp:clienthome')
        else:
            return "/login/"


class AdminProductView(AdminRequiredMixin, ListView):
    template_name = "admintemplates/adminproduct.html"
    model = Product
    context_object_name = "products"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['allcategorys'] = ProductCategory.objects.filter()

        if 'category' in self.request.GET:
            a = self.request.GET['category']
            if a == "all":
                context['allcategories'] = ProductCategory.objects.filter()
            else:
                context['allcategories'] = ProductCategory.objects.filter(id=a)
        # else:
        #   context['allcategories']= ProductCategory.objects.filter()

        return context


# API VIews

class ApiProductLCView(ListCreateAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class ApiProductRUDView(RetrieveUpdateDestroyAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
