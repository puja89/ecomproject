from django.urls import path
from .views import *

app_name = 'ecomapp'
urlpatterns = [
    # clients
    path('', ClientHomeView.as_view(), name="clienthome"),
    path('product/<int:pk>/productdetail',
         ClientProductDetailView.as_view(), name="clientproductdetail"),
    path('review-<pro_id>/', ClientReviewView.as_view(), name="clientreview"),
    path('review-<pro_id>/<int:pk>/delete-productreview',
         ClientReviewDeleteView.as_view(), name="clientreviewdelete"),
    path('my-cart/', MyCartView.as_view(), name="mycart"),
    path('add-to-cart/', AddToCartView.as_view(), name="addtocart"),
    path('manage-cart/', ManageCartView.as_view(), name="managecart"),

    path('checkout/', OrderCreateView.as_view(), name="ordercreate"),
    # Customer Registration
    path('register/', RegistrationView.as_view(), name="registration"),

    # admin
    path('ecom-admin/', AdminHomeView.as_view(), name="adminhome"),
    path('login/', LoginView.as_view(), name="login"),
    path('logout/', LogoutView.as_view(), name="logout"),
    path('ecom-admin/product/',
         AdminProductView.as_view(), name="adminproduct"),
    path('ecom-admin/order-<int:pk>/',
         AdminOrderDetailView.as_view(), name="adminorderdetail"),


    # API Views
    path('api/product/list-create/',
         ApiProductLCView.as_view(), name="apiproductlc"),
    path('api/product-<int:pk>/',
         ApiProductRUDView.as_view(), name="apiproductrud")

]
