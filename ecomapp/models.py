from django.template.defaultfilters import slugify
from django.db import models
from django.contrib.auth.models import User, Group
from .constants import *

# Create your models here.


class Timestamp(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    active = models.BooleanField(default=True)

    class Meta:
        abstract = True


class Admin(Timestamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='admin')

    def save(self, *args, **kwargs):
        grp, created = Group.objects.get_or_create(name='admin')
        self.user.groups.add(grp)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.name


class Customer(Timestamp):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    mobile = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200, null=True, blank=True)
    photo = models.ImageField(upload_to='customer', null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        grp, created = Group.objects.get_or_create(name='customer')
        self.user.groups.add(grp)

        super().save(*args, **kwargs)


class ProductCategory(Timestamp):
    title = models.CharField(max_length=200)
    slug = models.SlugField(unique=True, null=True, blank=True)
    image = models.ImageField(upload_to='category')
    root = models.ForeignKey(
        'self', on_delete=models.SET_NULL, null=True, blank=True)

    def save(self, *args, **kwargs):
        if not self.id:
            self.slug = slugify(self.title)

        super().save(*args, **kwargs)

    def __str__(self):
        return self.title


class Product(Timestamp):
    title = models.CharField(max_length=200)
    quantity = models.IntegerField(default=10)
    sku = models.CharField(max_length=200, unique=True)
    category = models.ForeignKey(ProductCategory, on_delete=models.CASCADE)
    description = models.TextField()
    mrp = models.DecimalField(max_digits=19, decimal_places=2)
    sp = models.DecimalField(max_digits=19, decimal_places=2)

    @property
    def discount(self):
        discount_pct = int((self.mrp - self.sp) / self.mrp * 100)
        return discount_pct

    @property
    def image(self):
        if self. productimage_set.all():
            a = self.productimage_set.first().image
        else:
            a = None
        return a

    def __str__(self):
        return self.title


class ProductImage(Timestamp):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image = models.ImageField(upload_to="productimage")

    def __str__(self):
        return "(" + self.product.title + ")"


Rating = (
    ("Excellent", "Excellent"),
    ("Good", "Good"),
    ("Average", "Average"),
    ("Poor", "Poor"),
    ("Not Good", "Not Good"),

)


class ProductReview(Timestamp):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    review = models.TextField()
    rating = models.CharField(max_length=50, choices=Rating)


class Cart(Timestamp):
    Customer = models.ForeignKey(
        Customer, on_delete=models.CASCADE, null=True, blank=True)
    total = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        return str(self.id)


class CartProduct(Timestamp):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    rate = models.DecimalField(max_digits=19, decimal_places=2)
    quantity = models.PositiveIntegerField()
    subtotal = models.DecimalField(max_digits=19, decimal_places=2)

    def __str__(self):
        return "(" + self.product.title + ")"


class Order(Timestamp):
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)
    subtotal = models.DecimalField(max_digits=19, decimal_places=2)
    discount = models.DecimalField(max_digits=19, decimal_places=2)
    total = models.DecimalField(max_digits=19, decimal_places=2)
    delivery_date = models.DateField(null=True, blank=True)
    order_status = models.CharField(max_length=50, choices=ORDER_STATUS)
    payment_method = models.CharField(max_length=50, choices=PAYMENT_METHOD)
    municipality = models.CharField(max_length=50, null=True, blank=True)
    street_address = models.CharField(max_length=50, null=True, blank=True)
    mobile = models.CharField(max_length=50)
    alt_mobile = models.CharField(max_length=50, null=True, blank=True)

    def __str__(self):
        return "Order id: " + str(self.id)
