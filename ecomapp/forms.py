from django import forms
from .models import *


class OrderForm(forms.ModelForm):
    class Meta:
        model = Order
        fields = ['payment_method', 'municipality',
                  'street_address', 'mobile', 'alt_mobile']


class LoginForm(forms.Form):
    username = forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control"}))


class CustomerRegistrationForm(forms.ModelForm):
    username = forms.CharField(widget=forms.TextInput(
        attrs={"class":"form-control"}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control"}))
    confirm_password = forms.CharField(widget=forms.PasswordInput(attrs={"class":"form-control"}))
    email = forms.CharField(widget=forms.EmailInput(attrs={"class":"form-control"}))
    mobile=forms.CharField(widget=forms.TextInput(attrs={"class":"form-control"}))
    name=forms.CharField(widget=forms.TextInput(
        attrs={"class":"form-control"}))
    address=forms.CharField(widget=forms.TextInput(
        attrs={"class":"form-control"}))

    class Meta:
        model = Customer
        fields = ['username', 'email', 'password', 'confirm_password',
                  'mobile', 'name', 'address', 'photo']


class ReviewForm(forms.ModelForm):
    class Meta:
        model = ProductReview
        fields = ['review', 'rating']
        widgets = {
            'review': forms.Textarea(attrs={
                'class': 'form-control',
                'rows': 3
            }),
            'rating': forms.Select(attrs={
                'class': 'form-control'
            })
        }
